resource "aws_route53_record" "subdomain" {
  zone_id = aws_route53_zone.zone.zone_id
  name    = join(".", ["${var.sub_domain}", "${var.root_domain_name}"])
  type    = "A"

  alias  {
    name                   = aws_cloudfront_distribution.subdomain.domain_name
    zone_id                = aws_cloudfront_distribution.subdomain.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "root" {
  zone_id = aws_route53_zone.zone.zone_id
  name    = var.root_domain_name
  type    = "A"

  alias  {
    name                   = aws_cloudfront_distribution.root.domain_name
    zone_id                = aws_cloudfront_distribution.root.hosted_zone_id
    evaluate_target_health = false
  }
}
