//resource "aws_acm_certificate" "hugo" {
//  domain_name               = var.root_domain_name
//  subject_alternative_names = ["*.${var.root_domain_name}"]
//  validation_method         = "EMAIL"
//  lifecycle {
//    create_before_destroy = true
//  }
//}
resource "aws_route53_zone" "zone" {
  name = var.root_domain_name
}